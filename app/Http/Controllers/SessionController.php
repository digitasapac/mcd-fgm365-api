<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class SessionController extends Controller
{
     /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function getSession(Request $request)
    {
        //$value = session('something');
        $data = $request->session()->all();
        return response()->json($data);
    }

    public function updateName($value, Request $request)
    {

        $data = $request->session()->put('name', $value);

        return response()->json($data, 200);
    }

}